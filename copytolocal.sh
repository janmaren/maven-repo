#!/bin/bash

find ~/.m2 -type d -name "*-SNAPSHOT" -exec rm -r "{}" \;
cp -R -n ~/.m2/repository/com/stmare ~/git/maven-repo/repository/com
cp -R -n ~/.m2/repository/org/eclipse ~/git/maven-repo/repository/org
cp -R -n ~/.m2/repository/org/stmare ~/git/maven-repo/repository/org
cp -R -n ~/.m2/repository/org/jfugue/ ~/git/maven-repo/repository/org
